
let someArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];


function getList (array, element = document.body) {

  let ul = document.createElement('ul');
    array.forEach(item => {
        const li = document.createElement('li');
        li.textContent = item;
        ul.append(li);
      });
      element.append(ul);
};

getList(someArray);

// додаткове завдання, але без таймеру
setTimeout(() => document.querySelector('ul').remove(), 3000);

